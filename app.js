var url = "mongodb://localhost:27017/webfingerprint";
var url0 = "mongodb://localhost:27017/";
var MongoClient = require('mongodb').MongoClient;
var express = require("express");
var app = express();
app.set('view engine', 'ejs');
var port = 3000;
var bodyParser = require('body-parser');
const path = require('path');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//body parser middleware
app.use(express.static(path.join(__dirname, '/')));

var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect(url);
var nameSchema = new mongoose.Schema({
    fpweb: String,
    detail: String
});
var User = mongoose.model("WebFp", nameSchema);


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

app.get("/fp", (req, res) => {
  var colision ;
  var fingers  ;
  var visiteur ;
  MongoClient.connect(url0, function(err, db) {
    if (err) throw err;
    var dbo = db.db("webfingerprint");
    dbo.collection("fingerprints").find({}).toArray(function(err, result) {
      if (err) throw err;
      fingers = result ;
      db.close();
    });
    dbo.collection("colisions").find({}).toArray(function(err, result) {
      if (err) throw err;
      colision = result ;
    });
    dbo.collection("visiteur").find({}).toArray(function(err, result) {
      if (err) throw err;
      visiteur = result;
     
    var data ={
      visiteursCount : visiteur.length,
      fingersCount   : fingers.length,
      colisionsCount : colision.length
     }
res.send(data)
//res.sendFile(__dirname + "/views/chartx.html", {visiteur:visiteur,fingers:fingers,colision:colision});
    });
  });

});


app.get("/fps", (req, res) => {
  var colision ;
  var fingers  ;
  var visiteur ;
  MongoClient.connect(url0, function(err, db) {
    if (err) throw err;
    var dbo = db.db("webfingerprint");
    dbo.collection("fingerprints").find({}).toArray(function(err, result) {
      if (err) throw err;
      fingers = result ;
      db.close();
    });
    dbo.collection("colisions").find({}).toArray(function(err, result) {
      if (err) throw err;
      colision = result ;
    });
    dbo.collection("visiteur").find({}).toArray(function(err, result) {
      if (err) throw err;
      visiteur = result;
     
    var data ={
      visiteursCount : visiteur,
      fingersCount   : fingers,
      colisionsCount : colision
     }
res.send(data)
//res.sendFile(__dirname + "/views/chartx.html", {visiteur:visiteur,fingers:fingers,colision:colision});
    });
  });

});


app.post("/fp", (req, res) => {
  MongoClient.connect(url0, function(err, db) {
    if (err) throw err;
    var dbo = db.db("webfingerprint");
    var myobj = { fpweb: req.body[0].fpweb , detail: req.body[0].detail };
    var myvisiteur = { fpweb: req.body[0].fpweb , platforme: req.body[0].platforme, Jour : req.body[0].Jour, time: new Date() };
    var query = { fpweb: req.body[0].fpweb };

    dbo.collection("visiteur").insertOne(myvisiteur, function(err, res) {
      if (err) throw err;
    });
    dbo.collection("fingerprints").find(query).toArray(function(err, result) {
      if (err) throw err;
      if (result.length<1) {
        dbo.collection("fingerprints").insertOne(myobj, function(err, res) {
          if (err) throw err;
          console.log("1 document inserted");
        });
      } else {
        if(result[0].detail != req.body[0].detail ){
          dbo.collection("fingerprints").insertOne(myobj, function(err, res) {
            if (err) throw err;
            console.log("vous etre pas seul ");
            dbo.collection("colisions").insertOne(myvisiteur, function(err, res) {
              if (err) throw err;
            });
          });
        }else{
          console.log('you are unique');
        }
      }
      
    db.close();
    });
/*     dbo.collection("fingerprints").insertOne(myobj, function(err, res) {
      if (err) throw err;
      console.log("1 document inserted");
      db.close();
    }); */
  });
});




app.listen(port, () => {
    console.log("Server listening on port " + port);
});