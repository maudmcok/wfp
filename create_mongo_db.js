var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/webfingerprint";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  console.log("Database created!");
  var dbo = db.db("webfingerprint");
  dbo.createCollection("fingerprints", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
  });
  dbo.createCollection("visiteur", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
   
  });
  dbo.createCollection("colisions", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
  });
});
