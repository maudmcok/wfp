

## Reqiere 
- NPM
- Mongodb
## Installation

- NPM: `npm i`
- NPM: `node create_mongo_db.js`

## Run
- NPM: `node app.js`
## Usage

- Site de récolte : ​`http://maudmcok.no-ip.fr:3000` 
- Site  Statistique: ​`http://maudmcok.no-ip.fr:3000/fp`
- Site  Visualisation Données ​`http://maudmcok.no-ip.fr:3000/fps`

### Tests

Unit tests are in `specs/specs.js`

`npm test` to launch the tests, it requires phanomjs install

To run the tests in the browser, launch `spec_runner.html`


## Other


#### License: MIT or Apache, whichever you prefer

[npm-link]: https://www.npmjs.com/package/fingerprintjs2
